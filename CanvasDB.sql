USE [master]
GO
/****** Object:  Database [Canvas]    Script Date: 19/02/2020 13:56:29 ******/
CREATE DATABASE [Canvas]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Canvas', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Canvas.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Canvas_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Canvas_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Canvas] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Canvas].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Canvas] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Canvas] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Canvas] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Canvas] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Canvas] SET ARITHABORT OFF 
GO
ALTER DATABASE [Canvas] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Canvas] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Canvas] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Canvas] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Canvas] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Canvas] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Canvas] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Canvas] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Canvas] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Canvas] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Canvas] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Canvas] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Canvas] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Canvas] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Canvas] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Canvas] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Canvas] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Canvas] SET RECOVERY FULL 
GO
ALTER DATABASE [Canvas] SET  MULTI_USER 
GO
ALTER DATABASE [Canvas] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Canvas] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Canvas] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Canvas] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Canvas] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Canvas', N'ON'
GO
ALTER DATABASE [Canvas] SET QUERY_STORE = OFF
GO
USE [Canvas]
GO
/****** Object:  User [NT AUTHORITY\SYSTEM]    Script Date: 19/02/2020 13:56:30 ******/
CREATE USER [NT AUTHORITY\SYSTEM] FOR LOGIN [NT AUTHORITY\SYSTEM] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Drawing]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Drawing](
	[drawingId] [smallint] IDENTITY(1,1) NOT NULL,
	[pictureId] [smallint] NULL,
	[lineColor] [varchar](20) NULL,
	[fillColor] [varchar](20) NULL,
	[comment] [varchar](50) NULL,
	[userId] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[drawingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Line]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Line](
	[lineId] [smallint] IDENTITY(1,1) NOT NULL,
	[startPointId] [smallint] NULL,
	[endPointId] [smallint] NULL,
	[drawingId] [smallint] NULL,
	[isActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[lineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Picture]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Picture](
	[pictureId] [smallint] IDENTITY(1,1) NOT NULL,
	[pictureUrl] [varchar](max) NULL,
	[pictureName] [varchar](20) NULL,
	[descriptions] [varchar](50) NULL,
	[isActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[pictureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PicturesForUser]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PicturesForUser](
	[PictureUserId] [smallint] IDENTITY(1,1) NOT NULL,
	[userId] [smallint] NULL,
	[userTypeId] [smallint] NULL,
	[pictureId] [smallint] NULL,
	[isActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[PictureUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Point]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Point](
	[pointId] [smallint] IDENTITY(1,1) NOT NULL,
	[X] [smallint] NULL,
	[Y] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[pointId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shape]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shape](
	[shapeId] [smallint] IDENTITY(1,1) NOT NULL,
	[shapeTypeId] [smallint] NULL,
	[radiusX] [smallint] NULL,
	[radiusY] [smallint] NULL,
	[centerPointId] [smallint] NULL,
	[drawingId] [smallint] NULL,
	[isActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[shapeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShapeType]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShapeType](
	[shapeTypeId] [smallint] IDENTITY(1,1) NOT NULL,
	[descriptions] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[shapeTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[userId] [smallint] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](20) NULL,
	[userPassword] [varchar](20) NULL,
	[emailAddress] [varchar](50) NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK__Users__CB9A1CFF9987116C] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserType]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserType](
	[userTypeId] [smallint] IDENTITY(1,1) NOT NULL,
	[descriptions] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[userTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Drawing] ON 

INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (1, 1, N'rrr', N'rr', N'ssss', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (2, 1, N'ggg', N'ggg', N'gfds', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (3, 1, N'rrr', N'rr', N'ssss', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (4, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (5, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (6, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (7, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (8, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (9, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (10, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (11, 2, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (12, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (13, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (14, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (15, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (16, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (17, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (18, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (19, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (20, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (21, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (22, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (23, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (24, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (25, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (26, 2, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (27, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (28, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (29, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (30, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (31, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (32, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (33, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (34, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (35, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (36, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (37, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (38, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (39, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (40, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (41, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (42, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (43, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (44, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (45, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (46, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (47, 1, N'red', N'red', N'ffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (48, 2, N'aaa', N'aaa', N'bnm', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (49, 2, N'aaa', N'aaa', N'ffffffffffffffffffffff', 1)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (50, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (51, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (52, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (53, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (54, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (55, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (56, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (57, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (58, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (59, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (60, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (61, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (62, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (63, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (64, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (65, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (66, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (67, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (68, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (69, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (70, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (71, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (72, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (73, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (74, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (75, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (76, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (77, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (78, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (79, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (80, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (81, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (82, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (83, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (84, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (85, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (86, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (87, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (88, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (89, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (90, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (91, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (92, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (93, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (94, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (95, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (96, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (97, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (98, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (99, 1, N'', N'', N'', 3)
GO
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (100, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (101, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (102, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (103, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (104, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (105, 2, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (130, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (131, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (132, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (133, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (134, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (135, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (136, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (137, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (138, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (139, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (140, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (141, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (142, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (143, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (144, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (145, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (146, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (147, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (148, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (149, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (150, 3, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (151, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (152, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (153, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (154, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (155, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (156, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (157, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (158, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (159, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (160, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (161, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (162, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (163, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (164, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (165, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (166, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (167, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (168, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (169, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (170, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (171, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (172, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (173, 1, N'', N'', N'', 3)
INSERT [dbo].[Drawing] ([drawingId], [pictureId], [lineColor], [fillColor], [comment], [userId]) VALUES (174, 1, N'', N'', N'', 3)
SET IDENTITY_INSERT [dbo].[Drawing] OFF
SET IDENTITY_INSERT [dbo].[Line] ON 

INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (1, 1, 2, 1, 0)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (2, 3, 4, 2, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (3, 1, 3, 3, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (4, 5, 6, 4, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (5, 7, 8, 5, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (6, 9, 10, 6, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (7, 11, 12, 7, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (8, 13, 14, 8, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (9, 15, 16, 9, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (10, 17, 18, 10, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (11, 5, 10, 11, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (12, 19, 20, 12, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (13, 21, 22, 13, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (14, 23, 24, 14, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (15, 25, 26, 15, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (16, 27, 28, 16, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (17, 29, 30, 17, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (18, 31, 32, 18, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (19, 33, 34, 19, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (20, 35, 36, 20, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (21, 37, 38, 21, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (22, 39, 40, 22, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (23, 41, 42, 23, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (24, 43, 44, 24, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (25, 45, 46, 25, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (26, 47, 48, 27, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (27, 49, 50, 28, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (28, 51, 52, 29, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (29, 53, 54, 30, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (30, 55, 56, 31, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (31, 57, 58, 32, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (32, 59, 60, 33, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (33, 61, 62, 34, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (34, 63, 64, 35, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (35, 65, 66, 36, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (36, 67, 68, 37, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (37, 69, 70, 38, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (38, 72, 73, 40, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (39, 75, 76, 42, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (40, 78, 79, 44, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (41, 81, 82, 46, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (42, 84, 85, 48, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (43, 86, 87, 49, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (44, 108, 109, 70, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (45, 111, 112, 72, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (46, 114, 115, 74, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (47, 184, 185, 143, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (48, 186, 187, 144, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (49, 188, 189, 145, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (50, 190, 191, 146, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (51, 192, 193, 147, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (52, 194, 195, 148, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (53, 196, 197, 149, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (54, 198, 199, 150, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (55, 200, 201, 151, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (56, 202, 203, 152, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (57, 205, 206, 154, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (58, 208, 209, 156, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (59, 211, 212, 158, 1)
INSERT [dbo].[Line] ([lineId], [startPointId], [endPointId], [drawingId], [isActive]) VALUES (60, 214, 215, 160, 1)
SET IDENTITY_INSERT [dbo].[Line] OFF
SET IDENTITY_INSERT [dbo].[Picture] ON 

INSERT [dbo].[Picture] ([pictureId], [pictureUrl], [pictureName], [descriptions], [isActive]) VALUES (1, N'https://localhost:44330/Images/ים4.jpg', N'dfghj', N'asdfghj', 1)
INSERT [dbo].[Picture] ([pictureId], [pictureUrl], [pictureName], [descriptions], [isActive]) VALUES (2, N'https://localhost:44330/Images/ים3.jpg', N'nnnnn', N'mmmmm', 1)
INSERT [dbo].[Picture] ([pictureId], [pictureUrl], [pictureName], [descriptions], [isActive]) VALUES (3, N'https://localhost:44330/Images/logo1.png', N'logo1.png', N'', NULL)
INSERT [dbo].[Picture] ([pictureId], [pictureUrl], [pictureName], [descriptions], [isActive]) VALUES (5, N'https://localhost:44330/Images/logo new.png', N'logo new.png', N'', NULL)
SET IDENTITY_INSERT [dbo].[Picture] OFF
SET IDENTITY_INSERT [dbo].[PicturesForUser] ON 

INSERT [dbo].[PicturesForUser] ([PictureUserId], [userId], [userTypeId], [pictureId], [isActive]) VALUES (1, 3, 1, 1, 1)
INSERT [dbo].[PicturesForUser] ([PictureUserId], [userId], [userTypeId], [pictureId], [isActive]) VALUES (2, 3, 1, 2, 1)
INSERT [dbo].[PicturesForUser] ([PictureUserId], [userId], [userTypeId], [pictureId], [isActive]) VALUES (3, 3, 1, 3, 1)
INSERT [dbo].[PicturesForUser] ([PictureUserId], [userId], [userTypeId], [pictureId], [isActive]) VALUES (4, 3, 1, NULL, 1)
INSERT [dbo].[PicturesForUser] ([PictureUserId], [userId], [userTypeId], [pictureId], [isActive]) VALUES (5, 3, 1, 5, 1)
SET IDENTITY_INSERT [dbo].[PicturesForUser] OFF
SET IDENTITY_INSERT [dbo].[Point] ON 

INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (1, 5, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (2, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (3, 2, 9)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (4, 15, 30)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (5, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (6, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (7, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (8, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (9, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (10, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (11, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (12, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (13, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (14, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (15, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (16, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (17, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (18, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (19, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (20, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (21, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (22, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (23, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (24, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (25, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (26, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (27, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (28, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (29, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (30, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (31, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (32, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (33, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (34, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (35, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (36, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (37, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (38, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (39, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (40, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (41, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (42, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (43, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (44, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (45, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (46, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (47, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (48, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (49, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (50, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (51, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (52, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (53, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (54, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (55, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (56, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (57, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (58, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (59, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (60, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (61, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (62, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (63, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (64, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (65, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (66, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (67, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (68, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (69, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (70, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (71, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (72, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (73, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (74, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (75, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (76, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (77, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (78, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (79, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (80, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (81, 9, 10)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (82, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (83, 5, 6)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (84, 66, 44)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (85, 5, 22)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (86, 66, 44)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (87, 5, 22)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (88, 0, 0)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (89, 90, 0)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (90, 0, 0)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (91, 262, 177)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (92, 486, 302)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (93, 287, 424)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (94, 610, 155)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (95, 402, 216)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (96, 165, 324)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (97, 262, 177)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (98, 486, 302)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (99, 287, 424)
GO
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (100, 610, 155)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (101, 402, 216)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (102, 165, 324)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (103, 217, 210)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (104, 449, 206)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (105, 509, 476)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (106, 469, 260)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (107, 267, 414)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (108, 456, 128)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (109, 76, 352)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (110, 469, 260)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (111, 456, 70)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (112, 93, 480)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (113, 267, 414)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (114, 698, 413)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (115, 68, 566)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (116, 419, 330)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (117, 328, 203)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (118, 419, 330)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (119, 328, 203)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (120, 120, 48)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (121, 0, 0)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (122, 500, 81)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (123, 235, 237)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (124, 458, 204)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (125, 259, 366)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (126, 235, 237)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (127, 458, 204)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (128, 259, 366)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (129, 235, 237)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (130, 458, 204)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (131, 259, 366)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (132, 235, 237)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (133, 458, 204)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (134, 259, 366)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (135, 235, 237)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (136, 458, 204)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (137, 235, 237)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (138, 458, 204)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (139, 259, 366)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (140, 235, 237)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (141, 458, 204)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (142, 259, 366)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (143, 277, 240)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (144, 478, 261)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (145, 253, 89)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (146, 417, 257)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (147, 439, 229)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (148, 439, 229)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (149, 657, 400)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (150, 657, 400)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (151, 217, 161)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (152, 217, 161)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (153, 217, 161)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (154, 375, 80)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (155, 375, 80)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (156, 375, 80)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (157, 375, 80)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (158, 323, 368)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (159, 323, 368)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (160, 323, 368)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (161, 323, 368)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (162, 242, 123)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (163, 242, 123)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (164, 242, 123)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (165, 242, 123)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (166, 130, 348)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (167, 130, 348)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (168, 130, 348)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (169, 130, 348)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (170, 130, 348)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (171, 326, 263)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (172, 584, 228)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (173, 584, 228)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (174, 507, 372)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (175, 507, 372)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (176, 448, 287)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (177, 179, 281)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (178, 179, 281)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (179, 358, 176)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (180, 358, 176)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (181, 312, 510)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (182, 312, 510)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (183, 312, 510)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (184, 390, 141)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (185, 603, 270)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (186, 390, 141)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (187, 603, 270)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (188, 289, 393)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (189, 223, 278)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (190, 289, 393)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (191, 223, 278)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (192, 558, 338)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (193, 215, 530)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (194, 558, 338)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (195, 215, 530)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (196, 108, 336)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (197, 298, 63)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (198, 108, 336)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (199, 298, 63)
GO
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (200, 214, 146)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (201, 312, 236)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (202, 214, 146)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (203, 312, 236)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (204, 272, 198)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (205, 214, 146)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (206, 312, 236)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (207, 272, 198)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (208, 153, 316)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (209, 300, 107)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (210, 162, 105)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (211, 153, 316)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (212, 300, 107)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (213, 162, 105)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (214, 153, 316)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (215, 300, 107)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (216, 0, 0)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (217, 0, 0)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (218, 249, 64)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (219, 249, 64)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (220, 327, 86)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (221, 327, 86)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (222, 339, 293)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (223, 339, 293)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (224, 339, 293)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (225, 339, 293)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (226, 64, 165)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (227, 64, 165)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (228, 64, 165)
INSERT [dbo].[Point] ([pointId], [X], [Y]) VALUES (229, 64, 165)
SET IDENTITY_INSERT [dbo].[Point] OFF
SET IDENTITY_INSERT [dbo].[Shape] ON 

INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (1, 1, 9, 10, 2, 26, 0)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (2, 1, 9, 10, 71, 39, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (3, 1, 9, 10, 74, 41, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (4, 1, 9, 10, 77, 43, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (5, 1, 9, 10, 80, 45, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (6, 1, 9, 10, 83, 47, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (7, 1, 0, 0, 88, 50, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (8, 1, 0, 0, 89, 51, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (9, 1, 0, 0, 90, 52, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (10, 1, 86, 34, 91, 53, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (11, 1, 47, 49, 92, 54, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (12, 1, 48, 59, 93, 55, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (13, 1, 68, 51, 94, 56, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (14, 1, 16, 109, 95, 57, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (15, 1, 43, 39, 96, 58, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (16, 1, 86, 34, 97, 59, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (17, 1, 47, 49, 98, 60, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (18, 1, 48, 59, 99, 61, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (19, 1, 68, 51, 100, 62, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (20, 1, 16, 109, 101, 63, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (21, 1, 43, 39, 102, 64, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (22, 1, 148, 78, 103, 65, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (23, 1, 141, 96, 104, 66, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (24, 1, 74, 91, 105, 67, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (25, 1, 68, 63, 106, 68, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (26, 2, 174, 98, 107, 69, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (27, 1, 68, 63, 100, 50, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (28, 2, 174, 98, 113, 73, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (36, 1, 61, 46, 123, 82, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (39, 1, 61, 46, 126, 85, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (42, 1, 61, 46, 129, 88, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (45, 1, 61, 46, 132, 91, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (48, 1, 61, 46, 135, 94, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (50, 1, 61, 46, 137, 96, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (51, 2, 59, 43, 138, 97, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (52, 2, 63, 37, 139, 98, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (53, 1, 61, 46, 140, 99, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (54, 2, 59, 43, 141, 100, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (55, 2, 63, 37, 142, 101, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (56, 1, 75, 58, 143, 102, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (57, 1, 66, 115, 144, 103, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (58, 2, 74, 34, 145, 104, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (59, 2, 75, 67, 146, 105, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (60, 1, 61, 34, 147, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (61, 1, 61, 34, 148, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (62, 1, 64, 60, 149, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (63, 1, 64, 60, 150, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (64, 2, 74, 41, 151, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (65, 2, 74, 41, 152, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (66, 2, 74, 41, 153, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (67, 2, 85, 23, 154, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (68, 2, 85, 23, 155, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (69, 2, 85, 23, 156, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (70, 2, 85, 23, 157, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (71, 2, 50, 82, 158, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (72, 2, 50, 82, 159, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (73, 2, 50, 82, 160, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (74, 2, 50, 82, 161, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (75, 2, 92, 75, 162, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (76, 2, 92, 75, 163, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (77, 2, 92, 75, 164, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (78, 2, 92, 75, 165, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (79, 1, 76, 65, 166, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (80, 1, 76, 65, 167, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (81, 1, 76, 65, 168, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (82, 1, 76, 65, 169, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (83, 1, 76, 65, 170, NULL, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (84, 1, 126, 81, 171, 130, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (85, 2, 68, 88, 172, 131, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (86, 2, 68, 88, 173, 132, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (87, 2, 21, 38, 174, 133, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (88, 2, 21, 38, 175, 134, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (89, 2, 120, 92, 176, 135, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (90, 1, 83, 77, 177, 136, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (91, 1, 83, 77, 178, 137, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (92, 1, 72, 31, 179, 138, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (93, 1, 72, 31, 180, 139, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (94, 2, 112, 41, 181, 140, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (95, 2, 112, 41, 182, 141, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (96, 2, 112, 41, 183, 142, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (97, 2, 36, 17, 204, 153, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (98, 2, 36, 17, 207, 155, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (99, 2, 72, 38, 210, 157, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (100, 2, 72, 38, 213, 159, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (101, 2, 0, 0, 216, 161, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (102, 2, 0, 0, 217, 162, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (103, 2, 0, 0, 218, 163, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (104, 2, 0, 0, 219, 164, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (105, 2, 66, 21, 220, 165, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (106, 2, 66, 21, 221, 166, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (107, 1, 76, 90, 222, 167, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (108, 1, 76, 90, 223, 168, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (109, 1, 76, 90, 224, 169, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (110, 1, 76, 90, 225, 170, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (111, 1, 42, 23, 226, 171, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (112, 1, 42, 23, 227, 172, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (113, 1, 42, 23, 228, 173, 1)
INSERT [dbo].[Shape] ([shapeId], [shapeTypeId], [radiusX], [radiusY], [centerPointId], [drawingId], [isActive]) VALUES (114, 1, 42, 23, 229, 174, 1)
SET IDENTITY_INSERT [dbo].[Shape] OFF
GO
SET IDENTITY_INSERT [dbo].[ShapeType] ON 

INSERT [dbo].[ShapeType] ([shapeTypeId], [descriptions]) VALUES (1, N'מלבן')
INSERT [dbo].[ShapeType] ([shapeTypeId], [descriptions]) VALUES (2, N'אליפסה')
SET IDENTITY_INSERT [dbo].[ShapeType] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([userId], [userName], [userPassword], [emailAddress], [isActive]) VALUES (1, N'michal', N'1234', N'michal57982@gmail.co', 1)
INSERT [dbo].[Users] ([userId], [userName], [userPassword], [emailAddress], [isActive]) VALUES (2, N'michal', N'1234', N'michal57982@gmail.co', 1)
INSERT [dbo].[Users] ([userId], [userName], [userPassword], [emailAddress], [isActive]) VALUES (3, N'Tzipi', N'123456', N'tz@tz', 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
SET IDENTITY_INSERT [dbo].[UserType] ON 

INSERT [dbo].[UserType] ([userTypeId], [descriptions]) VALUES (1, N'בעלים')
SET IDENTITY_INSERT [dbo].[UserType] OFF
ALTER TABLE [dbo].[Drawing]  WITH CHECK ADD FOREIGN KEY([pictureId])
REFERENCES [dbo].[Picture] ([pictureId])
GO
ALTER TABLE [dbo].[Drawing]  WITH CHECK ADD  CONSTRAINT [FK__Drawing__userId__20C1E124] FOREIGN KEY([userId])
REFERENCES [dbo].[Users] ([userId])
GO
ALTER TABLE [dbo].[Drawing] CHECK CONSTRAINT [FK__Drawing__userId__20C1E124]
GO
ALTER TABLE [dbo].[Line]  WITH CHECK ADD FOREIGN KEY([drawingId])
REFERENCES [dbo].[Drawing] ([drawingId])
GO
ALTER TABLE [dbo].[Line]  WITH CHECK ADD FOREIGN KEY([endPointId])
REFERENCES [dbo].[Point] ([pointId])
GO
ALTER TABLE [dbo].[Line]  WITH CHECK ADD FOREIGN KEY([startPointId])
REFERENCES [dbo].[Point] ([pointId])
GO
ALTER TABLE [dbo].[PicturesForUser]  WITH CHECK ADD FOREIGN KEY([pictureId])
REFERENCES [dbo].[Picture] ([pictureId])
GO
ALTER TABLE [dbo].[PicturesForUser]  WITH CHECK ADD  CONSTRAINT [FK__PicturesF__userI__25869641] FOREIGN KEY([userId])
REFERENCES [dbo].[Users] ([userId])
GO
ALTER TABLE [dbo].[PicturesForUser] CHECK CONSTRAINT [FK__PicturesF__userI__25869641]
GO
ALTER TABLE [dbo].[PicturesForUser]  WITH CHECK ADD FOREIGN KEY([userTypeId])
REFERENCES [dbo].[UserType] ([userTypeId])
GO
ALTER TABLE [dbo].[Shape]  WITH CHECK ADD FOREIGN KEY([centerPointId])
REFERENCES [dbo].[Point] ([pointId])
GO
ALTER TABLE [dbo].[Shape]  WITH CHECK ADD FOREIGN KEY([drawingId])
REFERENCES [dbo].[Drawing] ([drawingId])
GO
ALTER TABLE [dbo].[Shape]  WITH CHECK ADD  CONSTRAINT [FK_Shape_ShapeType] FOREIGN KEY([shapeTypeId])
REFERENCES [dbo].[ShapeType] ([shapeTypeId])
GO
ALTER TABLE [dbo].[Shape] CHECK CONSTRAINT [FK_Shape_ShapeType]
GO
/****** Object:  StoredProcedure [dbo].[AddDrawing]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddDrawing]
(
@pictureId smallint,
@lineColor varchar(20),
@fillColor varchar(20),
@comment varchar(50),
@userId smallint
)
as
begin
insert into [dbo].[Drawing] VALUES(@pictureId,@lineColor,@fillColor,@comment,@userId)
end
GO
/****** Object:  StoredProcedure [dbo].[AddLine]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddLine]
(
@pictureId smallint,
@lineColor varchar(20),
@fillColor varchar(20),
@comment varchar(50),
@userId smallint,
@startPointX smallint,
@startPointY smallint,
@endPointX smallint,
@endPointY smallint
)
as
begin
Exec AddDrawing @pictureId,@lineColor,@fillColor,@comment,@userId
declare @drawingId smallint
set @drawingId=@@IDENTITY
Exec AddPoint @startPointX,@startPointY
declare @startPointId smallint
set @startPointId=@@IDENTITY
Exec AddPoint @endPointX,@endPointY
declare @endPointId smallint
set @endPointId=@@IDENTITY
insert into [dbo].[Line] values(@startPointId,@endPointId,@drawingId,1)
select [lineId] from[dbo].[Line]
where [lineId]=@@IDENTITY
end
GO
/****** Object:  StoredProcedure [dbo].[addNewUser]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addNewUser]
(@userName varchar(20),
@password varchar(20),
@emailAddress varchar(20)) as
insert into dbo.Users values(@userName,@password,@emailAddress,1)
select [userId] from [dbo].[Users]
where userId=@@IDENTITY
GO
/****** Object:  StoredProcedure [dbo].[AddPicture]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddPicture]
(@userId int,@pictureUrl varchar(MAX), @pictureName nvarchar(50),@descriptions nvarchar(200))
as
begin
insert into [dbo].[Picture](pictureUrl,pictureName,descriptions)
values(@pictureUrl,@pictureName,@descriptions)
declare @id int
set @id= @@IDENTITY
declare @typeId int
select @typeId=userTypeId from [dbo].[UserType]
where [descriptions]='בעלים'
execute ShareNewUsers @userId,@typeId,@id
select [pictureId]from[dbo].[Picture]
where [pictureId]=@@IDENTITY
end
GO
/****** Object:  StoredProcedure [dbo].[AddPoint]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[AddPoint]
(
@x smallint,
@y smallint
)
as
begin
insert into [dbo].[Point] values(@x,@y)
end
GO
/****** Object:  StoredProcedure [dbo].[AddShape]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddShape]
(
@pictureId smallint,
@lineColor varchar(20),
@fillColor varchar(20),
@comment varchar(50),
@userId smallint,
@shapeTypeId smallint,
@radiusX smallint,
@radiusY smallint,
@centerPointX smallint,
@centerPointY smallint
)
as 
begin
Exec AddDrawing @pictureId,@lineColor,@fillColor,@comment,@userId
declare @drawingId smallint
set @drawingId=@@IDENTITY
Exec AddPoint @centerPointX,@centerPointY
insert into [dbo].[Shape] values(@shapeTypeId,@radiusX,@radiusY,@@IDENTITY,@drawingId,1)
select [shapeId] from[dbo].[Shape]
where [shapeId]=@@IDENTITY
end
GO
/****** Object:  StoredProcedure [dbo].[deleteLine]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[deleteLine]
(@lineId smallint)
as
begin
update dbo.Line
set [isActive]=0 where lineId=@lineId
end
GO
/****** Object:  StoredProcedure [dbo].[deleteShape]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[deleteShape]
(@shapeId smallint)
as
begin
update dbo.Shape
set [isActive]=0 where shapeId=@shapeId
end
GO
/****** Object:  StoredProcedure [dbo].[DeleteShareUsers]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DeleteShareUsers]
(@shareId smallint)
as
begin
update [dbo].[PicturesForUser]
set [isActive]=0 where [PictureUserId]=@shareId 
end
GO
/****** Object:  StoredProcedure [dbo].[deleteUser]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[deleteUser]
(@userId smallint)
 as
begin
update dbo.Users
set isActive= 0
where [userId]=@userId
end
GO
/****** Object:  StoredProcedure [dbo].[getLineByPictureId]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getLineByPictureId]
(@PictureId smallint )
as
begin
select [lineId],
sp.x as startPointX,
sp.y as startPointY,
ep.x as endPointX,
ep.y as endPointY,
[lineColor],
[fillColor],
[comment],
[userName],
d.[userId],
[pictureId]
from [dbo].[Line] inner join [dbo].[Point] as sp
on [startPointId]=sp.pointId
inner join [dbo].[Point] as ep
on endPointId=ep.pointId
inner join [dbo].[Drawing] as d
on [dbo].[Line].drawingId= d.drawingId
inner join [dbo].[Users] as u
on u.userId = d.userId
where d.[pictureId]=@PictureId
end



GO
/****** Object:  StoredProcedure [dbo].[GetPicturesPerUser]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetPicturesPerUser]
(@userId int)
as
select p.pictureId, pictureUrl,pictureName,p.descriptions ,pu.PictureUserId as userId,pu.userTypeId
from [dbo].[Picture] as p inner join
 [dbo].[PicturesForUser] as pu on p.[pictureId] =pu.pictureId
where pu.userId=@userId
GO
/****** Object:  StoredProcedure [dbo].[getShapesByPictureId]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[getShapesByPictureId]
(@PictureId smallint )
as
begin
select
[shapeId],
[radiusX],
[radiusY],
Shape.[shapeTypeId],
[X]as centerPointX,
[Y] as centerPointY,
[lineColor],
[fillColor],
[comment],
[userName],
Drawing.userId,
[pictureId]
from ((([dbo].[Shape] inner join [dbo].[Point]
on [dbo].[Point].[pointId] = [dbo].[Shape].[centerPointId])
inner join [dbo].[Drawing]
on [dbo].[Shape].drawingId=  [dbo].[Drawing].drawingId)
inner join [dbo].[ShapeType]
on [dbo].[Shape].shapeTypeId=[dbo].[ShapeType].shapeTypeId)
inner join  [dbo].[Users]
on [dbo].[Drawing].userId=  [dbo].[Users].userId
where [dbo].[Drawing].pictureId=@PictureId
end
GO
/****** Object:  StoredProcedure [dbo].[getShapeType]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[getShapeType]  as
select * from [dbo].[ShapeType]

GO
/****** Object:  StoredProcedure [dbo].[getUser]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getUser](@userName varchar(20),@password varchar(20)) as
begin
select [userId],[userName],[userPassword],[emailAddress] from dbo.Users
where dbo.Users.[userName] = @userName AND dbo.Users.[UserPassword] = @password And dbo.users.isActive=1
end
GO
/****** Object:  StoredProcedure [dbo].[GetUsersPerPicture]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetUsersPerPicture]
(@pictureId smallint)
as
begin
select [PictureUserId] as shareId, [userName],Users.userId,[userTypeId],PicturesForUser.pictureId as pictureId
from [dbo].[PicturesForUser] inner join [dbo].[Users]
on [dbo].[PicturesForUser].userId=[dbo].[Users].userId
where [pictureId]=@pictureId
end
GO
/****** Object:  StoredProcedure [dbo].[ShareNewUsers]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ShareNewUsers]
(@userId smallint,
@userTypeId smallint,
@pictureId smallint)
as
begin
insert into [dbo].[PicturesForUser] 
VALUES(@userId,@userTypeId,@pictureId,1)
select [PictureUserId] from[dbo].[PicturesForUser]
where [PictureUserId]=@@IDENTITY
end
GO
/****** Object:  StoredProcedure [dbo].[UpdateDrawing]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[UpdateDrawing](@drawId int,@lineColor varchar(20), @fillColor varchar(20),@comment varchar(50),@userId int )
as 
begin 
update [dbo].[Drawing]
set
fillColor=@fillColor,
lineColor=@lineColor,
comment=@comment,
userId=@userId
where drawingId=@drawId
end
GO
/****** Object:  StoredProcedure [dbo].[UpdateLine]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdateLine](
@lineId smallint,
@startPointX smallint,
@startPointY smallint,
@endPointX smallint,
@endPointY smallint,
@lineColor varchar(20),
@fillColor varchar(20),
@comment varchar(50),
@userId smallint,
@pictureId smallint
) as
begin
declare @drawingId int
select @drawingId=DrawingId from Line where lineId=@lineId
execute UpdateDrawing @drawingId,@lineColor,@fillColor,@comment,@userId
declare @startPointId int
select @startPointId=[startPointId] from Line where lineId=@lineId
execute UpdatePoint @startPointId,@startPointX,@startPointY
declare @endPointId int
select @endPointId=[endPointId] from Line where lineId=@lineId
execute UpdatePoint @endPointId,@endPointX,@endPointY
select lineId from [dbo].[Line] where lineId=@lineId
end 
GO
/****** Object:  StoredProcedure [dbo].[UpdatePicture]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdatePicture]
(
 @name nvarchar(50),@description nvarchar(200)
)
as
begin
update [dbo].[Picture]
set
[pictureName]=@name,
[descriptions]=@description
end
GO
/****** Object:  StoredProcedure [dbo].[updatePoint]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 create proc [dbo].[updatePoint](@pointId int,@x int, @y int)
as
begin
update [dbo].[Point]
set
X=@x,
Y=@y
where pointId=@pointId
end
GO
/****** Object:  StoredProcedure [dbo].[UpdateShape]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[UpdateShape](
@shapeId smallint,
@lineColor varchar(20),
@fillColor varchar(20),
@comment varchar(50),
@userId smallint,
@shapeTypeId smallint,
@radiusX smallint,
@radiusY smallint,
@centerPointX smallint,
@centerPointY smallint,
@pictureId smallint)
as
begin 
declare @drawId int
select @drawId=DrawingId from Shape where [shapeId]=@shapeId
execute UpdateDrawing @drawId,@lineColor,@fillColor,@comment,@userId
declare @pointId int
select @pointId=centerPointId from [dbo].[Shape] where shapeId=@shapeId
execute UpdatePoint @pointId,@centerPointX,@centerPointY

update [dbo].[Shape]
set
radiusX=@radiusX,
radiusY=@radiusY
end
GO
/****** Object:  StoredProcedure [dbo].[updateUser]    Script Date: 19/02/2020 13:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[updateUser](@userId smallint, @userName varchar(20),@password varchar(20),@emailAddress varchar(20)) as
begin
update dbo.Users
set [userName]=@userName,
[userPassword]=@password,
[emailAddress]=@emailAddress
where [userId]=@userId
end
GO
USE [master]
GO
ALTER DATABASE [Canvas] SET  READ_WRITE 
GO
